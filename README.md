## Purpose

Configuration for easy and fast VPN setup, when each node represent only itself. This setup doesn't include any automatic IP assignment, each node will have it's own, predefined, internal IP address.

---

## Setup

Each VPN should have a name. Choose a good one and replace any `<vpn>` tag with it.

For each node in the network follow steps:

1. Clone repository into directory `/etc/tinc/<vpn>`
```
git clone <url> /etc/tinc/<vpn>
```
2. Generate node keys with
```
tinc -n <vpn> init <node>
```
3. Now edit the `/etc/tinc/<vpn>/tinc.conf` file, add lines
```
LocalDiscovery = yes
ConnectTo = <root>
```
where `<root>` is a node name, accessible from the internet (can be used multiple times).
4. Edit the `/etc/tinc/<vpn>/hosts/<node>` file, add
```
IndirectData = yes
Subnet = <IP>/32
Address = <DNS name>
```
DNS name should be added only for nodes accessible from the internet. IP is a desired internal address of the node.
5. Copy host files of nodes specified in `ConnectTo` options. Optionally every host file can be added.
